from my_bot.enums import ChatWaitingStatus, TestStatus, GameType
from django.db.transaction import atomic

from my_bot import parse
from my_bot import helpers

from my_bot.models import Chat, ChatGame, ChatGameSession

ASK_FOR_ROWS = 'Введите количество строк в платежной матрице n >= 2: \n'
ASK_FOR_COLS = 'Введите количество столбцов в платежной матрице m >= 2: \n'
ASK_FOR_MATRIX = 'Введите платежную матрицу. Пример ввода:\n'
ASK_FOR_MATRIX += 'a_1_1, a_1_2, ... , a_1_m\n'
ASK_FOR_MATRIX += '............................................\n'
ASK_FOR_MATRIX += 'a_n_1, a_n_2, ... , a_n_m\n'

def on_start(chat_id, params=None):
    chat, created = Chat.objects.get_or_create(
        id=chat_id,
        defaults={'wait_for': ChatWaitingStatus.START}
    )
    
    hello_text = 'Выберите вид игры😉\n'
    begin(chat_id)

    return hello_text, {'reply_markup': helpers.keyboard}


def begin(chat_id, params=None):
    chat = Chat.objects.select_for_update().get(id=chat_id)
    session = ChatGameSession(chat_id=chat_id, status=TestStatus.OPEN)
    session.save()
    questions = ChatGame.objects.filter(session=session.id)
    question = ChatGame.make_one()
    question.session_id = session.id
    question.save()

@atomic
def on_text(chat_id, text):
    
    chat = Chat.objects.select_for_update().get(id=chat_id)

    question_query = ChatGame.objects.filter(
        session__chat=chat_id,
        session__status=TestStatus.OPEN
        )

    current_question = question_query.latest('create_date')

    if text == 'ИГРА В УСЛОВИЯХ НЕОПРЕДЕЛЕННОСТИ И РИСКА' or current_question.game == GameType.RISK:
        if chat.wait_for == ChatWaitingStatus.START:
            current_question.game = GameType.RISK
            current_question.save()
            chat.wait_for = ChatWaitingStatus.N
            chat.save()
            return ASK_FOR_ROWS
        elif chat.wait_for == ChatWaitingStatus.N:
            n = parse.parse_answer(text)
            if not n:
                return ASK_FOR_ROWS
            current_question.count_n = n
            chat.wait_for = ChatWaitingStatus.M
            chat.save()
            current_question.save()
            return ASK_FOR_COLS
        elif chat.wait_for == ChatWaitingStatus.M:
            m = parse.parse_answer(text)
            if not m:
                return ASK_FOR_COLS
            current_question.count_m = m
            chat.wait_for = ChatWaitingStatus.MATRIX
            chat.save()
            current_question.save()
            return ASK_FOR_MATRIX
        elif chat.wait_for == ChatWaitingStatus.MATRIX:
            A = []
            A = parse.parse_matrix(text, current_question.count_n, current_question.count_m)
            if not A:
                return ASK_FOR_MATRIX
            current_question.matrix = text
            chat.wait_for = ChatWaitingStatus.SOLVE
            chat.save()
            current_question.save()
            solution = helpers.task_close(chat, 1)
            return solution

    elif text == 'АНТАГОНИСТИЧЕСКАЯ ИГРА' or current_question.game == GameType.ZERO_SUM:
        if chat.wait_for == ChatWaitingStatus.START:
            current_question.game = GameType.ZERO_SUM
            current_question.save()
            chat.wait_for = ChatWaitingStatus.N
            chat.save()
            return ASK_FOR_ROWS
        elif chat.wait_for == ChatWaitingStatus.N:
            n = parse.parse_answer(text)
            if not n:
                return ASK_FOR_ROWS
            current_question.count_n = n
            chat.wait_for = ChatWaitingStatus.M
            chat.save()
            current_question.save()
            return ASK_FOR_COLS
        elif chat.wait_for == ChatWaitingStatus.M:
            m = parse.parse_answer(text)
            if not m:
                return ASK_FOR_COLS
            current_question.count_m = m
            chat.wait_for = ChatWaitingStatus.MATRIX
            chat.save()
            current_question.save()
            return ASK_FOR_MATRIX
        elif chat.wait_for == ChatWaitingStatus.MATRIX:
            A = []
            A = parse.parse_matrix(text, current_question.count_n, current_question.count_m)
            if not A:
                return ASK_FOR_MATRIX
            current_question.matrix = text
            chat.wait_for = ChatWaitingStatus.SOLVE
            chat.save()
            current_question.save()
            solution = helpers.task_close(chat, 2)
            return solution

    elif text == 'НЕАНТАГОНИСТИЧЕСКАЯ ИГРА':
        session = ChatGameSession.objects.get(
        chat_id=chat.id,
        status=TestStatus.OPEN
        )
        session.status = TestStatus.CLOSE
        session.save()
        chat.wait_for = ChatWaitingStatus.START
        chat.save()
        return 'Решение неантагонистических игр в процессе разработки😔'