from my_bot.models import ChatGameSession
from my_bot.enums import ChatWaitingStatus, TestStatus, GameType
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton

from my_bot import solution

keyboard = ReplyKeyboardMarkup(resize_keyboard=True, keyboard=[
            [
             KeyboardButton(text='ИГРА В УСЛОВИЯХ НЕОПРЕДЕЛЕННОСТИ И РИСКА'), 
            ],
            [
             KeyboardButton(text='АНТАГОНИСТИЧЕСКАЯ ИГРА')
            ],
            [
            KeyboardButton(text='НЕАНТАГОНИСТИЧЕСКАЯ ИГРА')
            ]
        ],
            one_time_keyboard=True
        )

def process_response(response):
    extra = {}
    if isinstance(response, (list, tuple)):
        text, extra = response[:2]
    else:
        text = response

    return text, extra


def task_close(chat, num_task):
    session = ChatGameSession.objects.get(
        chat_id=chat.id,
        status=TestStatus.OPEN
    )
    session.status = TestStatus.CLOSE

    result = session.chatgame_set.all()[0]

    if num_task == 1:
        session.solution_task = solution.task_1(result)
    elif num_task == 2:
        session.solution_task = solution.task_2(result)

    session.save()

    chat.wait_for = ChatWaitingStatus.START
    chat.save()
    
    text ='Решение задачи😊\n\n' + session.solution_task

    return text
