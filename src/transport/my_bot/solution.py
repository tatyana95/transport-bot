from my_bot import parse
from pulp import *

def find_valda(matrix):
    temp = []
    result = []
    for i in range(len(matrix)):
        min = matrix[i][0]
        for j in range(len(matrix[0])):
            if(matrix[i][j] < min):
                min = matrix[i][j]
        temp.append(min)
    
    max = temp[0]
    index = 0
    for i in range(len(temp)):
        if(temp[i] > max):
            max = temp[i]
            index = i
    result.append(index+1)
    result.append(max)
    return result

def find_laplas(matrix):
    temp = []
    result = []
    for i in range(len(matrix)):
        sum = 0
        for j in range(len(matrix[0])):
            sum+=matrix[i][j]
        temp.append(sum/len(matrix[0]))

    max = temp[0]
    index = 0
    for i in range(len(temp)):
        if(temp[i] > max):
            max = temp[i]
            index = i
    result.append(index+1)
    result.append(max)
    return result

def max_max(matrix):
    temp = []
    result = []
    for i in range(len(matrix)):
        max = matrix[i][0]
        for j in range(len(matrix[0])):
            if(matrix[i][j] > max):
                max = matrix[i][j]
        temp.append(max)
    
    max = temp[0]
    index = 0
    for i in range(len(temp)):
        if(temp[i] > max):
            max = temp[i]
            index = i
    result.append(index+1)
    result.append(max)
    return result

def find_gurvic(matrix):
       l = 1/2
       result_max = []
       result_min = []
       result_f = []
       result = []
       for i in range(len(matrix)):
              result_max.append(max(matrix[i]))
              result_min.append(min(matrix[i]))
       for i in range(len(result_max)):
              result_f.append(l*result_max[i] + (1-l)*result_min[i])

       max_res = result_f[0]
       index = 0
       for i in range(len(result_f)):
              if(result_f[i] > max_res):
                     max_res = result_f[i]
                     index = i
       result.append(index+1)
       result.append(max_res)
       return result

def find_sevidg(matrix):
       risk_matrix = []
       for i in range(len(matrix)):
              risk_matrix.append([0] * len(matrix[i]))
       max_st = []
       for j in range(len(matrix[0])):
              max_el = matrix[0][j]
              for i in range(len(matrix)):
                     if matrix[i][j] > max_el:
                            max_el = matrix[i][j]
              max_st.append(max_el)
        
       for i in range(len(risk_matrix)):
              for j in range(len(risk_matrix[0])):
                     risk_matrix[i][j] = max_st[j] - matrix[i][j]

       result_max = []
       result = []
       for i in range(len(risk_matrix)):
              result_max.append(max(risk_matrix[i]))

       min_res = result_max[0]
       index = 0
       for i in range(len(result_max)):
              if(result_max[i] < min_res):
                     min_res = result_max[i]
                     index = i
       result.append(index+1)
       result.append(min_res)
       return result


def task_1(result):
    n = result.count_n
    m = result.count_m
    A = parse.parse_matrix(result.matrix, n, m)

    crit_valda = find_valda(A)

    text = 'Оптимальной стратегией по критерию Вальда является {} стратегия. '.format(crit_valda[0])
    text += 'В этом случае максимальная прибыль составляет {}.\n\n'.format(crit_valda[1])

    crit_laplas = find_laplas(A)

    text += 'Оптимальной стратегией по критерию Лапласа является {} стратегия. '.format(crit_laplas[0])
    text += 'В этом случае максимальная прибыль составляет {}.\n\n'.format(round(crit_laplas[1], 4))

    crit_max = max_max(A)

    text += 'Оптимальной стратегией по критерию максимакса является {} стратегия. '.format(crit_max[0])
    text += 'В этом случае максимальная прибыль составляет {}.\n\n'.format(crit_max[1])

    crit_gurvic = find_gurvic(A)

    text += 'Оптимальной стратегией по критерию Гурвица является {} стратегия. '.format(crit_gurvic[0])
    text += 'В этом случае максимальная прибыль составляет {}.\n\n'.format(round(crit_gurvic[1], 4))

    crit_sevidg = find_sevidg(A)

    text += 'Оптимальной стратегией по критерию Сэвиджа является {} стратегия. '.format(crit_sevidg[0])
    text += 'В этом случае максимальная прибыль составляет {}.\n\n'.format(crit_sevidg[1])
   
    return text

def pure_strategy(matrix):
       min_res = []
       for i in range(len(matrix)):
              min_res.append(min(matrix[i]))
       v_1 = max(min_res)

       max_res = []

       for j in range(len(matrix[0])):
              max_el = matrix[0][j]
              for i in range(len(matrix)):
                     if matrix[i][j] > max_el:
                            max_el = matrix[i][j]
              max_res.append(max_el)

       v_2 = min(max_res)

       if v_1 == v_2:
              return v_1
       else:
              return None

def simplexMin(matrix, n, m):
    problem = pulp.LpProblem('0',pulp.LpMinimize)

    u_var = pulp.LpVariable.dicts(name="u", indexs=(range(n)), lowBound=0)

    problem+=pulp.lpSum(u_var[i] for i in range(n))

    for j in range(m):
        problem+=pulp.lpSum(matrix[i][j]*u_var[i] for i in range(n)) >= 1

    problem.solve()
    u=[]
    for variable in problem.variables():
        u.append(variable.varValue)
    sum_u = sum(u)
    x = []
    for item in u:
        x.append(item/sum_u)

    return x

def simplexMax(matrix, n, m):
    problem = pulp.LpProblem('0',pulp.LpMaximize)

    w_var = pulp.LpVariable.dicts(name="w", indexs=(range(m)), lowBound=0)

    problem+=pulp.lpSum(w_var[i] for i in range(m))

    for i in range(n):
        problem+=pulp.lpSum(matrix[i][j]*w_var[j] for j in range(m)) <= 1

    problem.solve()
    w=[]
    for variable in problem.variables():
        w.append(variable.varValue)

    sum_w = sum(w)
    y = []
    for item in w:
        y.append(item/sum_w)

    return y

def task_2(result):
    n = result.count_n
    m = result.count_m
    A = parse.parse_matrix(result.matrix, n, m)

    sol_pure = pure_strategy(A)
    if not sol_pure:
       text = 'Игра не имеет ситуации равновесия в чистых стратегиях. Ищем решение в смешанных стратегиях.\n\n'
       x = simplexMin(A, n, m)
       y = simplexMax(A, n, m)
       str_1 = "("
       for i in range(len(x)-1):
              str_1+=str(round(x[i], 4)) + ", "
       str_1+=str(round(x[len(x)-1], 4)) + ")"

       str_1 = 'Стратегия первого игрока x = {} .\n\n'.format(str_1)

       str_2 = "("
       for i in range(len(y)-1):
              str_2+=str(round(y[i], 4)) + ", "
       str_2+=str(round(y[len(y)-1], 4)) + ")"

       str_2 = 'Стратегия второго игрока y = {} .\n\n'.format(str_2)

       text+=str_1 + str_2

    else:
       text = 'Игра имеет ситуацию равновесия. Значение игры = {}\n\n'.format(sol_pure)

    return text

