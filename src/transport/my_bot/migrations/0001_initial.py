# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chat',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('wait_for', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='ChatGame',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('count_n', models.IntegerField(default=0)),
                ('count_m', models.IntegerField(default=0)),
                ('matrix', models.TextField(default='')),
                ('game', models.IntegerField(default=4)),
                ('create_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ChatGameSession',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('solution_task', models.TextField(default='')),
                ('status', models.IntegerField(default=2)),
                ('create_date', models.DateTimeField(null=True, auto_now_add=True)),
                ('chat', models.ForeignKey(to='my_bot.Chat')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='chatgame',
            name='session',
            field=models.ForeignKey(to='my_bot.ChatGameSession'),
        ),
    ]
