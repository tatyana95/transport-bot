def parse_answer(text):
    try:
        clear_value = int(text)
    except ValueError:
        return None

    if 2 <= clear_value <= 100:
        return clear_value

def parse_array(text, n):
    try:
        delete_space = text.replace(" ", "")
        simb = delete_space.split(',')
        array = []
        for i in range(len(simb)):
            elem = float(simb[i])
            array.append(elem)
    except ValueError:
        return None
    if len(array) == n:
        return array


def parse_matrix(text, n, m):
    try:
        C = []
        for i in range(n):
            C.append([0] * m)
        s = text.split('\n')
        flag = True
        count = 0
        for i in range(len(s)):
            delete_space = s[i].replace(" ", "")
            simb = delete_space.split(',')
            array = []
            for j in range(len(simb)):
                elem = float(simb[j])
                array.append(elem)
            if len(array)!=m:
                flag = False
            for k in range(len(array)):
                C[count][k] = array[k]
            count +=1
    except ValueError:
        return None
    if count == n and flag:
        return C
