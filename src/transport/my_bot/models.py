from django.db import models
from django_enumfield import enum

from my_bot.enums import ChatWaitingStatus, TestStatus, GameType


class BaseGameSession(models.Model):
    solution_task = models.TextField(default='')
    status = enum.EnumField(TestStatus, default=TestStatus.CLOSE)
    create_date = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        abstract = True

class MetaGame(models.base.ModelBase):
    def __new__(cls, name, bases, attrs):
        session_model = attrs.get('session_model')
        if session_model:
            attrs['session'] = models.ForeignKey(session_model)
        return super().__new__(cls, name, bases, attrs)

class BaseGame(models.Model, metaclass=MetaGame):
    session_model = None
    count_n = models.IntegerField(default=0)
    count_m = models.IntegerField(default=0)
    matrix = models.TextField(default='')
    game = enum.EnumField(GameType, default=GameType.NOT_SELECTED)

    class Meta:
        abstract = True

class Chat(models.Model):
    id = models.BigIntegerField(primary_key=True)
    wait_for = enum.EnumField(ChatWaitingStatus,
                              default=ChatWaitingStatus.START)

class ChatGameSession(BaseGameSession):
    """
    Для каждого чата должна быть одна открытая сессия
    """
    chat = models.ForeignKey(Chat)


class ChatGame(BaseGame):
    session_model = ChatGameSession
    create_date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def make_one(cls):
        return cls(
            count_n=0,
            count_m=0
        )
