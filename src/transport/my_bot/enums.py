from django_enumfield import enum

class ChatWaitingStatus(enum.Enum):
    START = 1
    N = 2
    M = 3
    MATRIX = 4
    SOLVE = 5

class TestStatus(enum.Enum):
    OPEN = 1
    CLOSE = 2

class GameType(enum.Enum):
	RISK = 1
	ZERO_SUM = 2
	NOT_ZERO_SUM = 3
	NOT_SELECTED = 4