Django==1.8
telepot==10.4
urllib3==1.20
django-enumfield==1.2.1
requests==2.13.0
pulp